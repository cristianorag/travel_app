import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:travel_app/pages/navpages/bar_item_page.dart';
import 'package:travel_app/pages/navpages/my_page.dart';
import 'package:travel_app/pages/navpages/search_page.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

List pages = [
  BarItemPage(),
  SearchPage(),
  MyPage(),
];

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(label: ("Home"), icon: Icon(Icons.apps)),
          BottomNavigationBarItem(
              label: ("Bar"), icon: Icon(Icons.bar_chart_sharp)),
          BottomNavigationBarItem(label: ("Search"), icon: Icon(Icons.search)),
          BottomNavigationBarItem(label: ("My"), icon: Icon(Icons.person)),
        ],
      ),
    );
  }
}
